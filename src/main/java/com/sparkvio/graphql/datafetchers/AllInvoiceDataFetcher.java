package com.sparkvio.graphql.datafetchers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sparkvio.spring.entity.InvoiceEntity;
import com.sparkvio.spring.jpa.IInvoiceEntityRepository;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class AllInvoiceDataFetcher implements DataFetcher<List<InvoiceEntity>>{

	@Autowired
	IInvoiceEntityRepository repository;
	
	@Override
	public List<InvoiceEntity> get(DataFetchingEnvironment dataFetchingEnvironment) {
		
		return repository.findAll();
	}

}
