package com.sparkvio.graphql.datafetchers;

import java.util.Optional;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.sparkvio.spring.entity.CustomerEntity;
import com.sparkvio.spring.entity.InvoiceEntity;
import com.sparkvio.spring.jpa.ICustomerEntityRepository;

public class CustomerResolver implements GraphQLResolver<CustomerEntity>{
	
	private ICustomerEntityRepository repository;
	
	public Optional<CustomerEntity> getCustomer(InvoiceEntity invoice) {
		
		return repository.findById(invoice.getCustomer().getCustomerId());
	}
}
