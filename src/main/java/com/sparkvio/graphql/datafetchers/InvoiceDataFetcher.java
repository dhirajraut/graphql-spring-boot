package com.sparkvio.graphql.datafetchers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sparkvio.spring.entity.InvoiceEntity;
import com.sparkvio.spring.jpa.IInvoiceEntityRepository;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class InvoiceDataFetcher implements DataFetcher<InvoiceEntity> {

	@Autowired
	IInvoiceEntityRepository repository;

	@Override
	public InvoiceEntity get(DataFetchingEnvironment dataFetchingEnvironment) {

		Long id = dataFetchingEnvironment.getArgument("id");
		return repository.getOne(id);
	}

}
