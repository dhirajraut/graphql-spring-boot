package com.sparkvio.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sparkvio.spring.service.InvoiceEntityService;

import graphql.ExecutionResult;

@RestController
@RequestMapping(path = "v1/" + RestConstants.URL_INVOICE_BASE)
public class InvoiceController {

	@Autowired
	InvoiceEntityService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Object> findAll(@RequestBody String query) {
		ExecutionResult executionResult = service.getGraphQL().execute(query);
		return new ResponseEntity<>(executionResult, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, path = RestConstants.URL_INVOICE_BYID)
	public ResponseEntity<Object> findById(@RequestBody String query) {
		ExecutionResult executionResult = service.getGraphQL().execute(query);
		return new ResponseEntity<>(executionResult, HttpStatus.OK);
	}
}

