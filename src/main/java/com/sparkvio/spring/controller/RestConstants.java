package com.sparkvio.spring.controller;

public interface RestConstants {

	public static final String URL_INVOICE_BASE = "/invoice";
	public static final String URL_INVOICE_BYID = "/{id}";
}
