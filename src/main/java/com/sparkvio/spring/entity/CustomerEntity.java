package com.sparkvio.spring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/* Lombok Annotations. */
@Getter
@Setter
@NoArgsConstructor
@ToString

/* JPA Annotations. */
@Entity
@Table (name = "Customer")
public class CustomerEntity {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "CustomerId")
	long customerId;

	@Column(name = "FirstName", nullable = false)
	String firstName;

	@Column(name = "LastName", nullable = false)
	String lastName;

	@Column(name = "Company")
	String company;

	@Column(name = "Address")
	String address;

	@Column(name = "City")
	String city;

	@Column(name = "State")
	String state;

	@Column(name = "Country")
	String country;

	@Column(name = "PostalCode")
	String postalCode;

	@Column(name = "Phone")
	String phone;

	@Column(name = "Fax")
	String fax;

	@Column(name = "Email", nullable = false)
	String email;

	@Column(name = "SupportRepId")
	int supportRepId;
}
