package com.sparkvio.spring.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/* Lombok Annotations. */
@Getter
@Setter
@NoArgsConstructor
@ToString

/* JPA Annotations. */
@Entity
@Table (name = "Invoice")
public class InvoiceEntity {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "InvoiceId")
	long invoiceId;

	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", referencedColumnName = "customerId")
    CustomerEntity customer;
    
    @Column(name = "InvoiceDate", nullable = false)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    Date invoiceDate;
    
    @Column(name = "BillingAddress")
    String billingAddress;
    
    @Column(name = "BillingCity")
    String billingCity;
    
    @Column(name = "BillingState")
    String billingState;
    
    @Column(name = "BillingCountry")
    String billingCountry;
    
    @Column(name = "BillingPostalCode")
    String billingPostalCode;
    
    @Column(name = "Total", nullable = false)
    Double total;
}
