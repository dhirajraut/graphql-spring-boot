package com.sparkvio.spring.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sparkvio.spring.entity.CustomerEntity;

/**
 * JPA Repository for entity.
 */
public interface ICustomerEntityRepository extends JpaRepository<CustomerEntity, Long> {
}
