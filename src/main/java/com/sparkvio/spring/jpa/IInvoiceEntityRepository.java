package com.sparkvio.spring.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sparkvio.spring.entity.InvoiceEntity;

/**
 * JPA Repository for entity.
 */
public interface IInvoiceEntityRepository extends JpaRepository<InvoiceEntity, Long> {

}
