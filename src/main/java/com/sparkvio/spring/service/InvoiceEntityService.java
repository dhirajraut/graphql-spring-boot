package com.sparkvio.spring.service;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.sparkvio.graphql.datafetchers.AllInvoiceDataFetcher;
import com.sparkvio.graphql.datafetchers.InvoiceDataFetcher;
import com.sparkvio.spring.jpa.IInvoiceEntityRepository;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

@Service
public class InvoiceEntityService {

	@Autowired
	IInvoiceEntityRepository repository;
	@Autowired
	ModelMapper modelMapper;
	
	/* GraphQL */
	@Value("classpath:invoice.graphql")
	Resource userSchemaResource;
	
	private GraphQL graphQL;
	public GraphQL getGraphQL() {
		return graphQL;
	}

	@Autowired
	private AllInvoiceDataFetcher allInvoiceDataFetcher;
	@Autowired
	private InvoiceDataFetcher invoiceDataFetcher;
	
	@PostConstruct
	private void loadSchema() throws IOException {
		File schemaFile = userSchemaResource.getFile();
		TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemaFile);
		RuntimeWiring wiring = buildRuntimeWiring();
		GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
		graphQL = GraphQL.newGraphQL(schema).build();
	}
	
	private RuntimeWiring buildRuntimeWiring() {
		
		return RuntimeWiring.newRuntimeWiring()
				.type("Query", typeWiring -> 
					typeWiring.dataFetcher("allInvoice", allInvoiceDataFetcher)
					.dataFetcher("invoiceById", invoiceDataFetcher)
				)
				.build();
	}
}
